const initialState = {};

export default function activeProductReducer(state = initialState, action) {
    switch(action.type) {
        case 'activeProduct/activeProductChanged': return action.payload;
        default: return state
    }
}

export const activeProductChanged = value => ({
    type: 'activeProduct/activeProductChanged',
    payload: value
})