const initialState = {
    loading: false,
    products: [],
    error: ''
}

export default function goodsReduser(state = initialState, action) {
    switch(action.type) {
        case 'goods/goodsLoading': return {
            ...state,
            loading: action.payload
        }
        case 'goods/goodsLoaded': return {
            ...state,
            products: action.payload,
            loading: false
        }
        case 'goods/goodsLoadedFailure': return {
            ...state,
            error: action.payload,
            loading: false
        }
        default: 
            return state;
    }
}

export const goodsLoading = value => ({
    type: 'goods/goodsLoading',
    payload: value
})

export const goodsLoaded = value => ({
    type: 'goods/goodsLoaded',
    payload: value
})

export const goodsLoadedFailure = value => ({
    type: 'goods/goodsLoadedFailure',
    payload: value
})

export const loadGoods = () => async dispatch => {
    dispatch(goodsLoading(true));
    try {
        const response = await fetch('products.json');
        const result = await response.json();
        dispatch(goodsLoaded(result));
    } catch(error) {
        dispatch(goodsLoadedFailure(error.message));
    }
}