const initialState = false;

export default function modalVisibleReducer(state = initialState, action) {
    switch(action.type) {
        case 'modalVisible/modalVisibleChanged': return !state
        default: return state;
    }
}

export const modalVisibleChanged = () => ({
    type: 'modalVisible/modalVisibleChanged'
})