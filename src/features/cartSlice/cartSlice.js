const initialState = [];

export default function cartReducer(state = initialState, action) {
    switch (action.type) {
        case 'cart/cartAdded': return action.payload
        case 'cart/cartRemoved': return action.payload
        default: return state
    }
}

export const cartAdded = value => ({
    type: 'cart/cartAdded',
    payload: value
})

export const cartRemoved = value => ({
    type: 'cart/cartRemoved',
    payload: value
})

export const addToCart = value => (dispatch, getState) => {
    const state = getState();
    const cart = [...state.store.cart, value];
    dispatch(cartAdded(cart));
}

export const removeFromCart = index => (dispatch, getState) => {
    const state = getState();
    const cart = state.store.cart;
    const arr = [...cart];
    arr.splice(index, 1);
    dispatch(cartRemoved(arr));
}
