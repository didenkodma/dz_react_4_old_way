const initialState = [];

export default function pickedOutReducer(state = initialState, action) {
    switch(action.type) {
        case 'pickedOut/pickedOutAdded': return action.payload
        case 'pickedOut/pickedOutRemoved': return action.payload
        default: return state
    }
}

export const pickedOutAdded = value => ({
    type: 'pickedOut/pickedOutAdded',
    payload: value
})

export const pickedOutRemoved = value => ({
    type: 'pickedOut/pickedOutRemoved',
    payload: value
})

export const addToPickedOut = value => (dispatch, getState) => {
    const state = getState();
    const pickedOut = [...state.store.pickedOut, value];
    dispatch(pickedOutAdded(pickedOut));
}

export const removeFromPickedOut = index => (dispatch, getState) => {
    const state = getState();
    const pickedOut = state.store.pickedOut;
    const arr = [...pickedOut];
    arr.splice(index, 1);
    dispatch(pickedOutRemoved(arr));
}