import { combineReducers } from "redux";
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import goodsReduser from "./features/goodsSlice/goodsSlice";
import modalVisibleReducer from "./features/modalVisibleSlice/modalVisibleSlice";
import cartReducer from "./features/cartSlice/cartSlice";
import pickedOutReducer from "./features/pickedOutSlice/pickedOutSlice";
import activeProductReducer from "./features/activeProductSlice/activeProductSlice";

const persistConfig = {
    key: 'root',
    storage
}

const storeReducer = combineReducers({
    cart: cartReducer,
    pickedOut: pickedOutReducer,
});

const persistedReducer = persistReducer(persistConfig, storeReducer);

const rootReducer = combineReducers({
    store: persistedReducer,
    goods: goodsReduser,
    modalVisible: modalVisibleReducer,
    activeProduct: activeProductReducer
})

export default rootReducer;