import "./Header.scss";
import HeaderTop from '../HeaderTop';
import HeaderBottom from '../HeaderBottom';

function Header() {

    return (
        <header className='header' id="header">

            <HeaderTop  />

            <HeaderBottom />

        </header>
    );
}

export default Header;