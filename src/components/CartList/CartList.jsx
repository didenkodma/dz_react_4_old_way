import "./CartList.scss";
import Product from '../Product';
import { shallowEqual, useSelector, useDispatch } from "react-redux";
import { modalVisibleChanged } from "../../features/modalVisibleSlice/modalVisibleSlice";
import { activeProductChanged } from "../../features/activeProductSlice/activeProductSlice";

function CartList() {

    const dispatch = useDispatch();
    const cart = useSelector(state => state.store.cart, shallowEqual);

    return (
        <div className='cart-section__list'>
            {cart.map((element, id) => {
                const { color, imgSrc, name, price, productCode, text } = element;
                const activeProduct = { id, color, imgSrc, name, price, productCode, text };
                const clickDeleteHandler = () => {
                    dispatch(activeProductChanged(activeProduct))
                    dispatch(modalVisibleChanged());
                }
                return <div key={id} className="cart-section__list-wrapper">
                    <Product isCart={true} color={color} imgSrc={imgSrc} name={name} price={price} productCode={productCode} text={text} />
                    <button className='cart-section__close-btn' onClick={clickDeleteHandler}></button>
                </div>
            })}
        </div>
    );
}


export default CartList;